﻿using UnityEngine;
using UnityEngine.UI;

public class DropDownMenu : MonoBehaviour
{
    //Prebaf
    private GameObject dropDownMenuGameObject;

    private Button fichaTecnicaButton;
    private Button creditosButton;
    private Button glosarioButton;
    private Button inicioButton;


    //private GameObject popUpContainterGameObject;
    private GameObject imgFichaTecnicaGameObject;
    private GameObject imgCreditosGameObject;
    private GameObject imgGlosarioGameObject;

    //Animacion
    private Animator menuAnimator;
    //Scripts Ayuda
    private Menu_Oda4 menuScript;
    private bool openMenu;

    //Sonidos
    private SoundManager soundManager;

    //Botones
    private Button toggleMenuButton;
    private Button closePopUpButton;

    // Use this for initialization
    private void Start()
    {
        //Script de ayuda
        menuScript = transform.GetComponent<Menu_Oda4>();

        //Sonidos
        soundManager = transform.GetComponent<SoundManager>();

        dropDownMenuGameObject = transform.FindChild("dropDownMenu").gameObject;
        dropDownMenuGameObject.GetComponent<Canvas>().worldCamera = Camera.main;
        toggleMenuButton = dropDownMenuGameObject.transform.GetChild(0).GetComponent<Button>();
        menuAnimator = dropDownMenuGameObject.transform.GetChild(0).GetChild(0).GetComponent<Animator>();
        closePopUpButton = dropDownMenuGameObject.transform.GetChild(1).GetComponent<Button>();
        imgFichaTecnicaGameObject = closePopUpButton.transform.GetChild(0).GetChild(0).gameObject;
        imgCreditosGameObject = closePopUpButton.transform.GetChild(0).GetChild(1).gameObject;
        imgGlosarioGameObject = closePopUpButton.transform.GetChild(0).GetChild(2).gameObject;
        fichaTecnicaButton = menuAnimator.transform.GetChild(0).GetComponent<Button>();
        creditosButton = menuAnimator.transform.GetChild(1).GetComponent<Button>();
        glosarioButton = menuAnimator.transform.GetChild(2).GetComponent<Button>();
        inicioButton = menuAnimator.transform.GetChild(3).GetComponent<Button>();
        toggleMenuButton.onClick.AddListener(ToggleMenu);
        closePopUpButton.onClick.AddListener(ClosePopUps);

        fichaTecnicaButton.onClick.AddListener(delegate { OpenPopUp("Ficha_Tecnica"); });
        creditosButton.onClick.AddListener(delegate { OpenPopUp("Creditos"); });
        glosarioButton.onClick.AddListener(delegate { OpenPopUp("Glosario"); });
        inicioButton.onClick.AddListener(delegate { GoToInicio(); });
    }

    private void Update()
    {
		if (menuScript.inicio.activeSelf || menuScript.retos[3].activeSelf) toggleMenuButton.gameObject.SetActive(false);
		else toggleMenuButton.gameObject.SetActive(true);
		if (menuScript.menu.activeSelf) inicioButton.gameObject.SetActive(false);
    	else inicioButton.gameObject.SetActive(true);
}

    private void GoToInicio()
    {
        irMenu();
        ToggleMenu();
    }

    private void OpenPopUp(string popUpName)
    {
        closePopUpButton.gameObject.SetActive(true);
        if (popUpName == "Ficha_Tecnica")
            imgFichaTecnicaGameObject.SetActive(true);
        else if (popUpName == "Creditos")
            imgCreditosGameObject.SetActive(true);
        else if (popUpName == "Glosario")
            imgGlosarioGameObject.SetActive(true);
    }


    private void ClosePopUps()
    {
        imgFichaTecnicaGameObject.SetActive(false);
        imgCreditosGameObject.SetActive(false);
        imgGlosarioGameObject.SetActive(false);
        closePopUpButton.gameObject.SetActive(false);
    }


    private void ToggleMenu()
    {
        openMenu = !openMenu;
        menuAnimator.SetBool("Opened", openMenu);
    }

    public void irMenu()
    {
        menuScript.abrirMenu();
        soundManager.playSound("Fondo_piano", 0);
    }
}