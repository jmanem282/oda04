﻿using UnityEngine;
using UnityEngine.UI;

public class Inicio_Oda4 : MonoBehaviour
{
    
    //Botones
    private Button btnEntrar;

    //Pantallas
    private GameObject inicio;
    private GameObject menu;

    //Sonidos
    private SoundManager soundManager;

    // Use this for initialization
    private void Start()
    {
        //Inicializar Player Prefs
        //PlayerPrefs.SetInt("mascarasODA1_ODA4", 0);

        //Sonidos
        soundManager = transform.GetComponent<SoundManager>();
        soundManager.playSound("Fondo_piano", 0);

        //Pantallas
        inicio = transform.FindChild("Inicio").gameObject;
        inicio.GetComponent<Canvas>().worldCamera = Camera.main;
        menu = transform.FindChild("Menu").gameObject;
        menu.GetComponent<Canvas>().worldCamera = Camera.main;


        //Boton
        btnEntrar = inicio.transform.FindChild("btnGoMenu").GetComponent<Button>();

        //Funciones boton
        btnEntrar.onClick.AddListener(delegate { abrirMenu(); });
    }

    private void abrirMenu()
    {
        inicio.SetActive(false);
        menu.SetActive(true);
    }
}