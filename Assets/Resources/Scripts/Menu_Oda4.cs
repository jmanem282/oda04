﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Menu_Oda4 : MonoBehaviour
{
    //Botones
    private Button btnReto1;
    private Button btnReto2;
    private Button btnReto3;
    private Button btnRetoFinal;

	[HideInInspector] public GameObject inicio;
    [HideInInspector] public GameObject menu;
    [HideInInspector] public GameObject Clon;


    [HideInInspector] public List<GameObject> retos = new List<GameObject>();

    private void Awake()
    {
        //Pantallas
		inicio = transform.FindChild("Inicio").gameObject;
        menu = transform.FindChild("Menu").gameObject;
        retos.Add(transform.FindChild("Reto1").gameObject);
        retos.Add(transform.FindChild("Reto2").gameObject);
        retos.Add(transform.FindChild("Reto3").gameObject);
        retos.Add(transform.FindChild("RetoFinal").gameObject);
    }

    // Use this for initialization
    private void Start()
    {
        //Asignación de camara
        for (var index = 0; index < retos.Count; index++) retos[index].GetComponent<Canvas>().worldCamera = Camera.main;

        //Botones
        btnReto1 = menu.transform.FindChild("btnReto1").GetComponent<Button>();
        btnReto2 = menu.transform.FindChild("btnReto2").GetComponent<Button>();
        btnReto3 = menu.transform.FindChild("btnReto3").GetComponent<Button>();
        btnRetoFinal = menu.transform.FindChild("btnRetoFinal").GetComponent<Button>();

        //Funciones boton
        btnReto1.onClick.AddListener(delegate { abrirReto(1); });

        btnReto2.onClick.AddListener(delegate { abrirReto(2); });

        btnReto3.onClick.AddListener(delegate { abrirReto(3); });

        btnRetoFinal.onClick.AddListener(delegate { abrirReto(4); });
    }

    //Funcion botones elegir reto
    public void abrirReto(int reto)
    {
        if (reto < 5)
        {            
            Clon = Instantiate(this.gameObject);
            Clon.SetActive(false);            
        }
        else
        {
            reto = reto - 4;
        }
        
        menu.SetActive(false);
        desactivarRetos();

        switch (reto)
        {
            case 1:                
                retos[0].SetActive(true);
                break;
            case 2:
                retos[1].SetActive(true);
                break;
            case 3:
                retos[2].SetActive(true);
                break;
            case 4:
                retos[3].SetActive(true);
                PlayerPrefs.SetInt("mascarasODA1_ODA4", 0);
                StartCoroutine(GetComponent<RetoFinal_Oda4>().secuenciaEventos("iniciarEvaluacion", 0.0f));
                break;
        }
    }

    public void abrirMenu()
    {
        Destroy(this.gameObject);
        Clon.name = "ODA4";
        Clon.SetActive(true);
        desactivarRetos();
        menu.SetActive(true);
    }

    private void desactivarRetos()
    {
        for (var index = 0; index < retos.Count; index++) retos[index].SetActive(false);
    }
}