﻿using System;
using UnityEngine;

public class MyTimer : MonoBehaviour
{
    [HideInInspector] public float remainingTime;

    [HideInInspector] public float secondsCount;

    [HideInInspector] private float stopTime;

    [HideInInspector] public bool timerEnable;

    [HideInInspector] public string timerText;

    private bool timerWatchEnable;

    private void Update()
    {
        UpdateTimerUI();
    }

    //call this on update
    public void UpdateTimerUI()
    {
        if (timerWatchEnable)
        {
            secondsCount += Time.deltaTime;
            timerText = FormatTime((int) secondsCount);
        }
        else if (timerEnable)
        {
            secondsCount += Time.deltaTime;
            remainingTime = stopTime - secondsCount;
            timerText = FormatTime((int) remainingTime);
//            Debug.Log(remainingTime);
        }
        if (0 >= remainingTime)
            StopTimer();
    }


    public bool isEnable()
    {
        return timerEnable;
    }

    public void ResetMyTimer()
    {
        StopTimeWatch();
    }

    public string FormatTime(int seconds)
    {
        var t = TimeSpan.FromSeconds(seconds);
        var result = string.Format("{0:D2}:{1:D2}", t.Minutes, t.Seconds);
        return result;
    }


    public void StartTimer(float stopTime)
    {
        this.stopTime = stopTime;
        remainingTime = stopTime;
        secondsCount = 0;
        timerEnable = true;
    }

    public void StopTimer()
    {
        timerEnable = false;
    }

    public void ContinueTimer()
    {
        timerEnable = true;
    }

    public void StopTimeWatch()
    {
        timerWatchEnable = false;
    }

    public void StartTimeWatch()
    {
        timerWatchEnable = true;
    }
}