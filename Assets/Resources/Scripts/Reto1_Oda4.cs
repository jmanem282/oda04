﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Reto1_Oda4 : MonoBehaviour
{
    private int banderaPuerta;
    private Button btnAceptarAcierto;
    private Button btnAceptarError1;
    private Button btnAceptarError2;
    private Button btnAceptarFinReto;

    //Botones
    private Button btnEntrar;
    private Button btnAceptarIndicacion;
    private Button btnPuerta1;
    private Button btnPuerta2;
    private Button btnPuerta3;
    private Button btnPuerta4;
    private Button btnPuerta5;
    private Button btnOpcionA;
    private Button btnOpcionB;
    private Button btnOpcionC;

    //Fondos de puertas
    private GameObject fondoPuerta1;
    private GameObject fondoPuerta2;
    private GameObject fondoPuerta3;
    private GameObject fondoPuerta4;
    private GameObject fondoPuerta5;

    //Variables del reto
    private GameObject mascaraFeliz;
    private int mascarasReto = 0;
    private int mascarasTotal;
    private Text mascarasText;

    //Scripts de ayuda
    private Menu_Oda4 menuScript;

    //PopUps
    private GameObject popIndicacion;
    private GameObject popAcierto;
    private GameObject popError1;
    private GameObject popError2;
    private GameObject popFinReto;
    private readonly List<GameObject> preguntas = new List<GameObject>();
    private List<int> preguntasRandom = new List<int>();

    //Pantallas
    private readonly List<GameObject> puertas = new List<GameObject>();
    private readonly int[] respuestas = {1, 3, 2, 2, 1, 2, 3, 1, 3, 3};

    //Sonidos
    private SoundManager soundManager;

    //Puntuación
    private GameObject puntuacion;


    // Use this for initialization
    private void Start()
    {
        //Scripts de ayuda
        menuScript = transform.GetComponent<Menu_Oda4>();

        //Sonidos
        soundManager = transform.GetComponent<SoundManager>();

        //Puntuación
        puntuacion = menuScript.retos[0].transform.FindChild("puntuacion").gameObject;
        mascarasText = puntuacion.transform.FindChild("Text").GetComponent<Text>();

        //Mascara
        mascaraFeliz = menuScript.retos[0].transform.FindChild("MascaraFeliz").gameObject;

        //PopUps
        popIndicacion = menuScript.retos[0].transform.FindChild("popIndicacion").gameObject;
        popFinReto = menuScript.retos[0].transform.FindChild("popFinReto").gameObject;
        preguntas.Add(menuScript.retos[0].transform.FindChild("Pregunta1").gameObject);
        preguntas.Add(menuScript.retos[0].transform.FindChild("Pregunta2").gameObject);
        preguntas.Add(menuScript.retos[0].transform.FindChild("Pregunta3").gameObject);
        preguntas.Add(menuScript.retos[0].transform.FindChild("Pregunta4").gameObject);
        preguntas.Add(menuScript.retos[0].transform.FindChild("Pregunta5").gameObject);
        preguntas.Add(menuScript.retos[0].transform.FindChild("Pregunta6").gameObject);
        preguntas.Add(menuScript.retos[0].transform.FindChild("Pregunta7").gameObject);
        preguntas.Add(menuScript.retos[0].transform.FindChild("Pregunta8").gameObject);
        preguntas.Add(menuScript.retos[0].transform.FindChild("Pregunta9").gameObject);
        preguntas.Add(menuScript.retos[0].transform.FindChild("Pregunta10").gameObject);

        //Puertas
        puertas.Add(menuScript.retos[0].transform.FindChild("Puerta1").gameObject);
        puertas.Add(menuScript.retos[0].transform.FindChild("Puerta2").gameObject);
        puertas.Add(menuScript.retos[0].transform.FindChild("Puerta3").gameObject);
        puertas.Add(menuScript.retos[0].transform.FindChild("Puerta4").gameObject);
        puertas.Add(menuScript.retos[0].transform.FindChild("Puerta5").gameObject);

        //Fondos de puerta
        fondoPuerta1 = puertas[0].transform.FindChild("fondo2").gameObject;
        fondoPuerta2 = puertas[1].transform.FindChild("fondo2").gameObject;
        fondoPuerta3 = puertas[2].transform.FindChild("fondo2").gameObject;
        fondoPuerta4 = puertas[3].transform.FindChild("fondo2").gameObject;
        fondoPuerta5 = puertas[4].transform.FindChild("fondo2").gameObject;

        //Botones
        btnEntrar = menuScript.retos[0].transform.FindChild("btnEntrar").GetComponent<Button>();
        btnAceptarIndicacion = popIndicacion.transform.FindChild("btnAceptar").GetComponent<Button>();
        btnAceptarFinReto = popFinReto.transform.FindChild("btnAceptar").GetComponent<Button>();
        btnPuerta1 = puertas[0].transform.FindChild("btnPuerta").GetComponent<Button>();
        btnPuerta2 = puertas[1].transform.FindChild("btnPuerta").GetComponent<Button>();
        btnPuerta3 = puertas[2].transform.FindChild("btnPuerta").GetComponent<Button>();
        btnPuerta4 = puertas[3].transform.FindChild("btnPuerta").GetComponent<Button>();
        btnPuerta5 = puertas[4].transform.FindChild("btnPuerta").GetComponent<Button>();

        //Lista para almacenar preguntas a mostrar
        preguntasRandom = new List<int>();

        //Funciones boton
        btnEntrar.onClick.AddListener(delegate { abrirPuerta(1); });
        btnAceptarIndicacion.onClick.AddListener(delegate { cerrarPopUp("AceptarIndicacion"); });
        btnAceptarFinReto.onClick.AddListener(delegate
        {
            cerrarPopUp("AceptarFinReto");
            menuScript.abrirReto(6);
        });
        btnPuerta1.onClick.AddListener(delegate { entrarPuerta(1); });
        btnPuerta2.onClick.AddListener(delegate { entrarPuerta(2); });
        btnPuerta3.onClick.AddListener(delegate { entrarPuerta(3); });
        btnPuerta4.onClick.AddListener(delegate { entrarPuerta(4); });
        btnPuerta5.onClick.AddListener(delegate { entrarPuerta(5); });
    }

    //Funcion botones elegir reto
    public void abrirPuerta(int puerta)
    {
        desactivarPuertas();

        switch (puerta)
        {
            case 1:
                puertas[0].SetActive(true);
                seleccionaPreguntas();
                obtenerPlayerPrefs();
                Invoke("mostrarIndicacion", 0.5f);
                break;
            case 2:
                puertas[1].SetActive(true);
                break;
            case 3:
                puertas[2].SetActive(true);
                break;
            case 4:
                puertas[3].SetActive(true);
                break;
            case 5:
                puertas[4].SetActive(true);
                break;
        }
    }

    //Seleccionar preguntas
    private void seleccionaPreguntas()
    {
        var i = 0;
        for (i = 0; i < 5; i++)
        {
            var numrandom = Random.Range(1, 11);

            if (preguntasRandom.Contains(numrandom)) i--;
            else preguntasRandom.Add(numrandom);
        }
    }

    //Desactivar puertas
    private void desactivarPuertas()
    {
        for (var index = 0; index < puertas.Count; index++) puertas[index].SetActive(false);
    }

    //Mostrar PopUp de Indicacion
    private void mostrarIndicacion()
    {
        soundManager.playSound("Sound_OpenPopUp_10", 1);
        popIndicacion.SetActive(true);
    }

    //Cerrar PopUps
    private void cerrarPopUp(string popUp)
    {
        soundManager.playSound("Sound_Cerrar_3", 1);
        switch (popUp)
        {
            case "AceptarIndicacion":
                popIndicacion.SetActive(false);
                break;
            case "AceptarFinReto":
                popFinReto.SetActive(false);
                break;
        }
    }

    //Mostrar contenido de la puerta
    public void entrarPuerta(int puerta)
    {
        desactivarFondos();

        switch (puerta)
        {
            case 1:
                fondoPuerta1.SetActive(true);
                banderaPuerta = 1;
                Invoke("mostrarPregunta", 2.0f);
                break;
            case 2:
                fondoPuerta2.SetActive(true);
                Invoke("mostrarPregunta", 2.0f);
                break;
            case 3:
                fondoPuerta3.SetActive(true);
                Invoke("mostrarPregunta", 2.0f);
                break;
            case 4:
                fondoPuerta4.SetActive(true);
                Invoke("mostrarPregunta", 2.0f);
                break;
            case 5:
                fondoPuerta5.SetActive(true);
                Invoke("mostrarPregunta", 2.0f);
                break;
        }
    }

    //Mostrar pregunta
    private void mostrarPregunta()
    {
        int index = 0, intentos = 0;

        desactivarPreguntas();
        index = preguntasRandom[banderaPuerta - 1] - 1;
        preguntas[index].SetActive(true);
        soundManager.playSound("Sound_OpenPopUp_10", 1);

        //PopUps
        popAcierto = preguntas[index].transform.FindChild("popAcierto").gameObject;
        popError1 = preguntas[index].transform.FindChild("popError1").gameObject;
        popError2 = preguntas[index].transform.FindChild("popError2").gameObject;

        //Botones
        btnOpcionA = preguntas[index].transform.FindChild("btnA").GetComponent<Button>();
        btnOpcionB = preguntas[index].transform.FindChild("btnB").GetComponent<Button>();
        btnOpcionC = preguntas[index].transform.FindChild("btnC").GetComponent<Button>();
        btnAceptarAcierto = popAcierto.transform.FindChild("Button").GetComponent<Button>();
        btnAceptarError1 = popError1.transform.FindChild("Button").GetComponent<Button>();
        btnAceptarError2 = popError2.transform.FindChild("Button").GetComponent<Button>();

        //Puntuación
        puntuacion.SetActive(true);
        mascarasText.text = mascarasTotal.ToString();

        btnOpcionA.onClick.AddListener(delegate
        {
            intentos++;
            evaluarPregunta(btnOpcionA, index, intentos);
        });
        btnOpcionB.onClick.AddListener(delegate
        {
            intentos++;
            evaluarPregunta(btnOpcionB, index, intentos);
        });
        btnOpcionC.onClick.AddListener(delegate
        {
            intentos++;
            evaluarPregunta(btnOpcionC, index, intentos);
        });
        btnAceptarAcierto.onClick.AddListener(delegate
        {
            popAcierto.SetActive(false);
            preguntas[index].SetActive(false);
            soundManager.playSound("Sound_cerrar_3", 1);
            mascarasTotal++;
            mascarasText.text = mascarasTotal.ToString();
            PlayerPrefs.SetInt("mascarasODA1_ODA4", mascarasTotal);
            mascaraFeliz.SetActive(true);
            Invoke("ocultarMascara", 1.5f);
            Invoke("siguientePuerta", 1.8f);
        });
        btnAceptarError1.onClick.AddListener(delegate
        {
            popError1.SetActive(false);
            soundManager.playSound("Sound_cerrar_3", 1);
        });
        btnAceptarError2.onClick.AddListener(delegate
        {
            popError2.SetActive(false);
            soundManager.playSound("Sound_cerrar_3", 1);
            siguientePuerta();
        });
    }

    //Evaluar pregunta
    private void evaluarPregunta(Button bot, int num, int intentos)
    {
        switch (bot.name)
        {
            case "btnA":
                if (1 == respuestas[num])
                {
                    //Mostrar PopUp de acierto
                    popAcierto.SetActive(true);
                    soundManager.playSound("Sound_correcto_10", 1);
                }
                else
                {
                    if (2 == intentos)
                    {
                        //Mostrar PopUp de error
                        popError2.SetActive(true);
                        soundManager.playSound("Sound_incorrecto_19", 1);
                    }
                    //Mostrar PopUp de ayuda
                    popError1.SetActive(true);
                    soundManager.playSound("Sound_incorrecto_19", 1);
                }
                break;
            case "btnB":
                if (2 == respuestas[num])
                {
                    //Mostrar PopUp de acierto
                    popAcierto.SetActive(true);
                    soundManager.playSound("Sound_correcto_10", 1);
                }
                else
                {
                    if (2 == intentos)
                    {
                        //Mostrar PopUp de error
                        popError2.SetActive(true);
                        soundManager.playSound("Sound_incorrecto_19", 1);
                    }
                    //Mostrar PopUp de ayuda
                    popError1.SetActive(true);
                    soundManager.playSound("Sound_incorrecto_19", 1);
                }
                break;
            case "btnC":
                if (3 == respuestas[num])
                {
                    //Mostrar PopUp de acierto
                    popAcierto.SetActive(true);
                    soundManager.playSound("Sound_correcto_10", 1);
                }
                else
                {
                    if (2 == intentos)
                    {
                        //Mostrar PopUp de error
                        popError2.SetActive(true);
                        soundManager.playSound("Sound_incorrecto_19", 1);
                    }
                    //Mostrar PopUp de ayuda
                    popError1.SetActive(true);
                    soundManager.playSound("Sound_incorrecto_19", 1);
                }
                break;
        }
    }

    //Desactivar preguntas
    private void desactivarPreguntas()
    {
        for (var index = 0; index < 10; index++)
            preguntas[index].SetActive(false);
    }

    //Desactivar fondos
    private void desactivarFondos()
    {
        fondoPuerta1.SetActive(false);
        fondoPuerta2.SetActive(false);
        fondoPuerta3.SetActive(false);
        fondoPuerta4.SetActive(false);
        fondoPuerta5.SetActive(false);
    }

    //Avanzar puerta o finalizar reto
    private void siguientePuerta()
    {
        puntuacion.SetActive(false);
        desactivarPreguntas();
        banderaPuerta++;
        if (banderaPuerta < 6)
        {
            abrirPuerta(banderaPuerta);
        }
        else
        {
            popFinReto.SetActive(true);
            soundManager.playSound("Sound_OpenPopUp_10", 1);
        }
    }

    private void obtenerPlayerPrefs()
    {
        mascarasTotal = PlayerPrefs.GetInt("mascarasODA1_ODA4");
    }

    private void ocultarMascara()
    {
        mascaraFeliz.SetActive(false);
    }
}