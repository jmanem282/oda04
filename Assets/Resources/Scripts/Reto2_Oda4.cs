﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class Reto2_Oda4 : MonoBehaviour
{
    private int banderaPersonaje;
    private int persActual;
    private int contPalabras;
    private int intentos = 1;
    private int mascarasTotal;

    //Animaciones
    private GameObject brillo1;
    private GameObject brillo2;
    private GameObject brillo3;
    private GameObject animLicuadora;
    private GameObject mascaraFeliz;

    //Botones
    private Button btnEntrar;
    private Button btnLicuar;
    private Button btnAceptarIndicacion;
    private Button btnAceptarAcierto;
    private Button btnAceptarError;
    private Button btnAceptarFinReto;

    [HideInInspector] public GameObject Clon;

    private MyTimer cronometro;
    private bool dragging;
    private bool leerUpdate = true;

    //Reloj
    private GameObject manecilla;

    //Variables del reto
    private Text mascarasText;

    //Scripts de ayuda
    private Menu_Oda4 menuScript;

    private readonly List<string> palabras = new List<string>
    {
        "palabra1",
        "palabra2",
        "palabra3",
        "palabra4",
        "palabra5",
        "palabra6",
        "palabra7",
        "palabra8",
        "palabra9",
        "palabra10"
    };

    //Pantallas
    private GameObject pantalla1;
    private readonly List<GameObject> personajes = new List<GameObject>();
    private List<int> personajesRandom = new List<int>();

    //PopUps
    private GameObject popIndicacion;
    private GameObject popAcierto;
    private GameObject popError;
    private GameObject popFinReto;
    private readonly List<List<string>> popUps = new List<List<string>>();
    private Vector3 posIni;

    //Para funcionalidad del dragging
    private GameObject posLicuadora;
    private GameObject elemento;
    private readonly List<List<string>> respuestas = new List<List<string>>();

    //Personajes de la pantalla principal
    private Image silueta1;
    private Image imagen1;
    private Image silueta2;
    private Image imagen2;
    private Image silueta3;
    private Image imagen3;

    //Lista de personajes
    private Image siluetaPer1;
    private Image imagenPer1;
    private Image siluetaPer2;
    private Image imagenPer2;
    private Image siluetaPer3;
    private Image imagenPer3;

    //Sonidos
    private SoundManager soundManager;

    //Texto de PopUps
    private Text txtAcierto;
    private Text txtError;


    // Use this for initialization
    private void Start()
    {
        //Scripts de ayuda
        menuScript = transform.GetComponent<Menu_Oda4>();

        //Sonidos
        soundManager = transform.GetComponent<SoundManager>();


        cronometro = transform.gameObject.GetComponent<MyTimer>();
        //cronometro.StartTimer (15.0f);


        //Pantalla
        pantalla1 = menuScript.retos[1].transform.FindChild("Pantalla1").gameObject;

        //Reloj
        manecilla = pantalla1.transform.FindChild("manecilla").gameObject;

        //Puntuación
        mascarasText = pantalla1.transform.FindChild("puntuacion").FindChild("Text").GetComponent<Text>();
        mascarasText.text = mascarasTotal.ToString();

        //PopUps
        popIndicacion = pantalla1.transform.FindChild("popIndicacion").gameObject;
        popAcierto = pantalla1.transform.FindChild("popAcierto").gameObject;
        popError = pantalla1.transform.FindChild("popError").gameObject;
        popFinReto = pantalla1.transform.FindChild("popFinReto").gameObject;

        //Texto para popups Acierto/Error de los 10 personajes
        popUps.Add(new List<string>
        {
            "Conoces muy bien las características de un hada madrina, ellas siempre ayudan a las princesas en apuros. ¡Nuestra obra de teatro será un éxito!",
            "Recuerda que el estereotipo que conocemos del hada madrina nos refiere a una figura que protege a los buenos. Vuelve a intentarlo."
        });
        popUps.Add(new List<string>
        {
            "¡Muy bien! Este personaje lo conoces a la perfección, las princesas son personajes que viven comúnmente en castillos y que normalmente son representadas con una belleza especial.",
            "Recuerda que el estereotipo de las princesas las describe como frágiles damas que pueden ser comparadas con las flores por su encantadora apariencia. Vuelve a intentarlo."
        });
        popUps.Add(new List<string>
        {
            "¡Bien hecho! Si necesitamos un príncipe en la obra tú lo conoces bien, está siempre dispuesto a rescatar una princesa en problemas.",
            "Recuerda que el estereotipo conocido sobre un príncipe enlista el valor y la bondad como elementos indispensables. Vuelve a intentarlo."
        });
        popUps.Add(new List<string>
        {
            "¡Acertaste! Una bruja que prepara brebajes y conjuros para interferir con la felicidad de los buenos, no le vendría nada mal a nuestra obra.",
            "No olvides que el esterotipo de las brujas no puede dejar de lado su transporte volador y su necesidad de quedarse con lo que les pertenece a otros utilizando la magia. Vuelve a intentarlo."
        });
        popUps.Add(new List<string>
        {
            "¡Correcto! Una egoísta y malvada madrastra será un ingrediente atractivo, estas villanas siempre buscan aprovecharse de la bondad de las heroínas en los cuentos.",
            "La madrastra representa algunos conflictos dentro de las historias, es un personaje que siempre busca un beneficio personal por encima del resto de los personajes.Vuelve a intentarlo."
        });
        popUps.Add(new List<string>
        {
            "¡Bien contestado! Un ogro, puede ser un villano ideal para nuestra obra, su grotesca joroba y su mal olor son la ideal muestra de la maldad.",
            "No olvides que la apariencia del ogro horroriza a todo el que lo ve y lo huele. Vuelve a intentarlo."
        });
        popUps.Add(new List<string>
        {
            "El lobo feroz  suele representar el miedo en medio de la noche. ¡Nuestra obra de teatro será un éxito!",
            "Recuerda el estereotipo del lobo que es un fiero y temible villano, con un gran apetito y que pone a temblar a una tierna abuelita. Vuelve a intentarlo."
        });
        popUps.Add(new List<string>
        {
            "¡Muy bien! Este personaje lo conoces a la perfección, los caballeros siempre estarán en búsqueda de la justicia bajo una pesada armadura.",
            "No olvides que el estereotipo de los caballeros los describe como valerosos hombres en busca de la justicia.Vuelve a intentarlo."
        });
        popUps.Add(new List<string>
        {
            "¡Bien hecho! Los dragones son monstruos alados siempre dispuestos a quemar a cualquiera que se atreva a rescatar a la princesa.",
            "Recuerda que el estereotipo del dragón integra su ardiente aliento y monstruoso parecer. Vuelve a intentarlo."
        });
        popUps.Add(new List<string>
        {
            "¡Acertaste! Un pirata no vendría nada mal para nuestra obra, estos ladrones en altamar, con su mal humor y su ambicioso anhelo de encontrar tesoros perdidos son villanos ideales.",
            "Recuerda que el estereotipo del pirata no puede dejar de lado su afilado garfio y una pata de palo, resultado de su constante enfrentamiento con otros marineros en búsqueda de valiosos tesoros.Vuelve a intentarlo."
        });

        //Imagenes para los 3 personajes principales
        silueta1 = pantalla1.transform.FindChild("silueta1").GetComponent<Image>();
        imagen1 = pantalla1.transform.FindChild("imagen1").GetComponent<Image>();
        silueta2 = pantalla1.transform.FindChild("silueta2").GetComponent<Image>();
        imagen2 = pantalla1.transform.FindChild("imagen2").GetComponent<Image>();
        silueta3 = pantalla1.transform.FindChild("silueta3").GetComponent<Image>();
        imagen3 = pantalla1.transform.FindChild("imagen3").GetComponent<Image>();

        //Animaciones
        brillo1 = pantalla1.transform.FindChild("brillo1").gameObject;
        brillo2 = pantalla1.transform.FindChild("brillo2").gameObject;
        brillo3 = pantalla1.transform.FindChild("brillo3").gameObject;
        animLicuadora = pantalla1.transform.FindChild("animLicuadora").gameObject;
        mascaraFeliz = pantalla1.transform.FindChild("MascaraFeliz").gameObject;

        //Posicion del vaso de licuadora
        posLicuadora = pantalla1.transform.FindChild("posLicuadora").gameObject;

        //Lista de personajes
        personajes.Add(pantalla1.transform.FindChild("personaje1").gameObject);
        personajes.Add(pantalla1.transform.FindChild("personaje2").gameObject);
        personajes.Add(pantalla1.transform.FindChild("personaje3").gameObject);
        personajes.Add(pantalla1.transform.FindChild("personaje4").gameObject);
        personajes.Add(pantalla1.transform.FindChild("personaje5").gameObject);
        personajes.Add(pantalla1.transform.FindChild("personaje6").gameObject);
        personajes.Add(pantalla1.transform.FindChild("personaje7").gameObject);
        personajes.Add(pantalla1.transform.FindChild("personaje8").gameObject);
        personajes.Add(pantalla1.transform.FindChild("personaje9").gameObject);
        personajes.Add(pantalla1.transform.FindChild("personaje10").gameObject);

        //Respuestas correctas para los 10 personajes
        respuestas.Add(new List<string> {"palabra1", "palabra4", "palabra5", "palabra6", "palabra8"});
        respuestas.Add(new List<string> {"palabra1", "palabra2", "palabra5", "palabra6", "palabra10"});
        respuestas.Add(new List<string> {"palabra4", "palabra6", "palabra7", "palabra8", "palabra10"});
        respuestas.Add(new List<string> {"palabra2", "palabra4", "palabra6", "palabra7", "palabra10"});
        respuestas.Add(new List<string> {"palabra2", "palabra3", "palabra6", "palabra7", "palabra9"});
        respuestas.Add(new List<string> {"palabra2", "palabra3", "palabra6", "palabra8", "palabra9"});
        respuestas.Add(new List<string> {"palabra1", "palabra3", "palabra5", "palabra8", "palabra9"});
        respuestas.Add(new List<string> {"palabra1", "palabra3", "palabra7", "palabra8", "palabra9"});
        respuestas.Add(new List<string> {"palabra3", "palabra4", "palabra6", "palabra9", "palabra10"});
        respuestas.Add(new List<string> {"palabra3", "palabra6", "palabra7", "palabra8", "palabra9"});

        //Botones
        btnEntrar = menuScript.retos[1].transform.FindChild("btnEntrar").GetComponent<Button>();
        btnLicuar = pantalla1.transform.FindChild("btnLicuar").GetComponent<Button>();
        btnAceptarIndicacion = popIndicacion.transform.FindChild("btnAceptar").GetComponent<Button>();
        btnAceptarAcierto = popAcierto.transform.FindChild("btnAceptar").GetComponent<Button>();
        btnAceptarError = popError.transform.FindChild("btnAceptar").GetComponent<Button>();
        btnAceptarFinReto = popFinReto.transform.FindChild("btnAceptar").GetComponent<Button>();

        //Textos de PopUps
        txtAcierto = popAcierto.transform.GetChild(0).GetChild(0).GetComponent<Text>();
        txtError = popError.transform.GetChild(0).GetChild(0).GetComponent<Text>();

        //Lista para almacenar personajes a mostrar
        personajesRandom = new List<int>();

        //Funciones boton
        btnEntrar.onClick.AddListener(delegate { mostrarPantalla1(); });
        btnLicuar.onClick.AddListener(delegate
        {
            licuarPalabras();
            Invoke("mostrarPopAcierto", 2f);
        });
        btnAceptarIndicacion.onClick.AddListener(delegate
        {
            cerrarPopUp("AceptarIndicacion");
            iniciarCronometro();
        });
        btnAceptarAcierto.onClick.AddListener(delegate
        {
            cerrarPopUp("AceptarAcierto");
            ocultarObjetos();
            mascarasTotal++;
            mascarasText.text = mascarasTotal.ToString();
            PlayerPrefs.SetInt("mascarasODA1_ODA4", mascarasTotal);
            mascaraFeliz.SetActive(true);
            Invoke("ocultarMascara", 1.5f);
            Invoke("cambiarPersonaje", 1.8f);
        });
        btnAceptarError.onClick.AddListener(delegate
        {
            cerrarPopUp("AceptarError");
            iniciarCronometro();
        });
        btnAceptarFinReto.onClick.AddListener(delegate
        {
            cerrarPopUp("AceptarFinReto");
            menuScript.abrirReto(7);
        });
    }

    private void Update()
    {
        if (pantalla1.activeInHierarchy)
        {
            manecilla.transform.eulerAngles = new Vector3(0, 0, -1*cronometro.remainingTime*6);

            if (0 <= cronometro.remainingTime)
            {
                if (Input.touchCount > 0)
                {
                    var pos = Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position);
                    var hit = Physics2D.Raycast(pos, Vector2.zero);
                    if (Input.GetTouch(0).phase == TouchPhase.Began)
                        try
                        {
                            if (palabras.Contains(hit.collider.gameObject.name))
                            {
                                elemento = hit.collider.gameObject;
                                posIni = hit.collider.gameObject.transform.localPosition;
                                dragging = true;
                            }
                        }
                        catch (Exception ex)
                        {
                        }
                    if (Input.GetTouch(0).phase == TouchPhase.Moved)
                        if (dragging)
                            elemento.transform.position = new Vector3(pos.x, pos.y + 0.5f, elemento.transform.position.z);
                    if (Input.GetTouch(0).phase == TouchPhase.Ended)
                        if (dragging)
                        {
                            if (
                            (Vector3.Distance(elemento.transform.localPosition, posLicuadora.transform.localPosition) <
                             80) && respuestas[persActual].Contains(elemento.name))
                            {
                                elemento.transform.localPosition = posLicuadora.transform.localPosition;
                                elemento.SetActive(false);
                                contPalabras++;
                                soundManager.playSound("Sound_correcto_10", 1);
                            }
                            else
                            {
                                elemento.transform.localPosition = posIni;
                                soundManager.playSound("Sound_incorrecto_19", 1);
                            }
                            if (5 == contPalabras)
                            {
                                cronometro.StopTimer();
                                soundManager.stopSound(2);
                                btnLicuar.gameObject.SetActive(true);
                            }
                            dragging = false;
                        }
                }
            }
            else
            {
                if (leerUpdate)
                {
                    soundManager.stopSound(2);
                    soundManager.playSound("Sound_tiempoOff", 1);
                    if (2 > intentos)
                    {
                        intentos++;
                        leerUpdate = false;
                        txtError.text = popUps[persActual][1];
                        popError.SetActive(true);
                    }
                    else
                    {
                        leerUpdate = false;
                        activarImagen();
                        cambiarPersonaje();
                    }
                }
            }
        }
    }

    //Funcion botones elegir reto

    //Seleccionar preguntas
    private void seleccionaPersonajes()
    {
        var i = 0;
        for (i = 0; i < 3; i++)
        {
            var numrandom = Random.Range(1, 11);

            if (personajesRandom.Contains(numrandom)) i--;
            else personajesRandom.Add(numrandom);
        }
    }

    //Desactivar personajes
    private void desactivarPersonajes()
    {
        for (var index = 0; index < personajes.Count; index++) personajes[index].SetActive(false);
    }

    //Mostrar PopUp de Indicacion
    private void mostrarIndicacion()
    {
        popIndicacion.SetActive(true);
        soundManager.playSound("Sound_OpenPopUp_10", 1);
    }

    //Cerrar PopUps
    private void cerrarPopUp(string popUp)
    {
        soundManager.playSound("Sound_Cerrar_3", 1);
        switch (popUp)
        {
            case "AceptarIndicacion":
                popIndicacion.SetActive(false);
                break;
            case "AceptarAcierto":
                popAcierto.SetActive(false);
                break;
            case "AceptarError":
                popError.SetActive(false);
                break;
            case "AceptarFinReto":
                popFinReto.SetActive(false);
                break;
        }
    }

    //Mostrar pantalla
    private void mostrarPantalla1()
    {
        obtenerPlayerPrefs();
        restablecerVariables();
        desactivarPersonajes();
        seleccionaPersonajes();
        asignarPersonajes();
        pantalla1.SetActive(true);
        mostrarPalabras();
        Invoke("mostrarIndicacion", 0.5f);
    }

    //Mostrar palabras del personaje actual
    private void mostrarPalabras()
    {
        banderaPersonaje++;
        persActual = personajesRandom[banderaPersonaje - 1] - 1;

        desactivarPersonajes();
        ocultarBrillos();
        mostrarBrillos();
        personajes[persActual].SetActive(true);
    }


    //Asignar personajes principales
    private void asignarPersonajes()
    {
        var index1 = personajesRandom[0] - 1;
        var index2 = personajesRandom[1] - 1;
        var index3 = personajesRandom[2] - 1;

        siluetaPer1 = personajes[index1].transform.FindChild("silueta").GetComponent<Image>();
        imagenPer1 = personajes[index1].transform.FindChild("imagen").GetComponent<Image>();
        siluetaPer2 = personajes[index2].transform.FindChild("silueta").GetComponent<Image>();
        imagenPer2 = personajes[index2].transform.FindChild("imagen").GetComponent<Image>();
        siluetaPer3 = personajes[index3].transform.FindChild("silueta").GetComponent<Image>();
        imagenPer3 = personajes[index3].transform.FindChild("imagen").GetComponent<Image>();

        silueta1.sprite = siluetaPer1.sprite;
        imagen1.sprite = imagenPer1.sprite;
        silueta2.sprite = siluetaPer2.sprite;
        imagen2.sprite = imagenPer2.sprite;
        silueta3.sprite = siluetaPer3.sprite;
        imagen3.sprite = imagenPer3.sprite;

        silueta1.SetNativeSize();
        imagen1.SetNativeSize();
        silueta2.SetNativeSize();
        imagen2.SetNativeSize();
        silueta3.SetNativeSize();
        imagen3.SetNativeSize();
    }

    private void ocultarBrillos()
    {
        brillo1.SetActive(false);
        brillo2.SetActive(false);
        brillo3.SetActive(false);
    }

    private void mostrarBrillos()
    {
        switch (banderaPersonaje)
        {
            case 1:
                brillo1.SetActive(true);
                break;
            case 2:
                brillo2.SetActive(true);
                break;
            case 3:
                brillo3.SetActive(true);
                break;
        }
    }

    private void restablecerVariables()
    {
        banderaPersonaje = 0;
        persActual = 0;
        contPalabras = 0;
        intentos = 1;
        mascarasText.text = mascarasTotal.ToString();
    }

    private void activarImagen()
    {
        switch (banderaPersonaje)
        {
            case 1:
                imagen1.gameObject.SetActive(true);
                break;
            case 2:
                imagen2.gameObject.SetActive(true);
                break;
            case 3:
                imagen3.gameObject.SetActive(true);
                break;
        }
    }

    private void iniciarCronometro()
    {
        cronometro.StartTimer(15.5f);
        soundManager.playSound("Sound_reloj", 2);
        leerUpdate = true;
    }

    private void cambiarPersonaje()
    {
        if (3 > banderaPersonaje)
        {
            contPalabras = 0;
            intentos = 1;
            mostrarPalabras();
            iniciarCronometro();
        }
        else
        {
            popFinReto.SetActive(true);
            soundManager.playSound("Sound_OpenPopUp_10", 1);
        }
    }

    private void licuarPalabras()
    {
        soundManager.playSound("Sound_licuadora", 1);
        animLicuadora.SetActive(true);
    }

    //Mostrar PopUps de licuadora
    private void mostrarPopAcierto()
    {
        txtAcierto.text = popUps[persActual][0];
        popAcierto.SetActive(true);
        soundManager.playSound("Sound_OpenPopUp_10", 1);
        activarImagen();
    }

    private void ocultarObjetos()
    {
        btnLicuar.gameObject.SetActive(false);
        animLicuadora.SetActive(false);
    }

    private void obtenerPlayerPrefs()
    {
        mascarasTotal = PlayerPrefs.GetInt("mascarasODA1_ODA4");
    }

    private void ocultarMascara()
    {
        mascaraFeliz.SetActive(false);
    }
}