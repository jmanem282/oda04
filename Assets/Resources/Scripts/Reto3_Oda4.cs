﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Reto3_Oda4 : MonoBehaviour
{
    private int banderaIndicacion;
    private int mascarasTotal;
    private readonly List<GameObject> baules = new List<GameObject>();

    //Botones
    private Button btnGuion;
    private Button btnAceptarIndicacionReto;
    private Button btnAceptarIndicacionBaul;
    private Button btnAceptarFinReto;
    private Button btnBaul1;
    private Button btnBaul2;
    private Button btnBaul3;
    private Button btnAceptarGuion;
    private Button btnAceptarTitulo;
    private Button btnAceptarPersonajes;
    private Button btnAceptarEscenario;
    private Button btnAceptarDialogos;
    private Button btnAceptarGuardar;
    private Button btnAceptarFelicidades;
    private Button btnAceptarCerca;
    private Button btnAceptarError;
    private Button btnZonaTitulo;
    private Button btnZonaPersonajes;
    private Button btnZonaEscenario;
    private Button btnZonaDialogos;
    private Button btnFlechaDer;
    private Button btnFlechaIzq;
    private Button btnFin;
    private Button btnGuardar;

    [HideInInspector] public GameObject Clon;

    //Hojas
    private GameObject hoja1;
    private GameObject hoja2;

    //Input Texts
    private InputField inpTitulo;
    private InputField inpPersonajes;
    private InputField inpEscenario;
    private InputField inpDialogos;
    private InputField inpDialogos2;

    //Puntuación
    private GameObject puntuacion;
    private GameObject mascaraFeliz;

    //Fondo pantalla guardar
    private GameObject fondoGuardar;

    //Variables del reto
    private Text mascarasText;

    //Scripts de ayuda
    private Menu_Oda4 menuScript;

    //PopUps
    private GameObject popIndicacionReto;
    private GameObject popIndicacionBaul;
    private GameObject popFinReto;
    private GameObject popElementosGuion;
    private GameObject popZonaTitulo;
    private GameObject popZonaPersonajes;
    private GameObject popZonaEscenario;
    private GameObject popZonaDialogos;
    private GameObject popGuardar;
    private GameObject popFelicidades;
    private GameObject popCerca;
    private GameObject popError;

    //Pantallas
    private GameObject seleccionarBaul;

    //Sonidos
    private SoundManager soundManager;

    //Captura de pantalla
    Texture2D texture1;
    private static AndroidJavaObject _activity;

    // Use this for initialization
    private void Start()
    {
        //Scripts de ayuda
        menuScript = transform.GetComponent<Menu_Oda4>();

        //Sonidos
        soundManager = transform.GetComponent<SoundManager>();

        //Mascara
        mascaraFeliz = menuScript.retos[2].transform.FindChild("MascaraFeliz").gameObject;

        //PopUps
        popIndicacionReto = menuScript.retos[2].transform.FindChild("popIndicacionReto").gameObject;
        popIndicacionBaul = menuScript.retos[2].transform.FindChild("popIndicacionBaul").gameObject;
        popFinReto = menuScript.retos[2].transform.FindChild("popFinReto").gameObject;


        //Baules
        seleccionarBaul = menuScript.retos[2].transform.FindChild("SeleccionarBaul").gameObject;
        baules.Add(menuScript.retos[2].transform.FindChild("Baul1").gameObject);
        baules.Add(menuScript.retos[2].transform.FindChild("Baul2").gameObject);
        baules.Add(menuScript.retos[2].transform.FindChild("Baul3").gameObject);


        //Botones
        btnAceptarIndicacionReto = popIndicacionReto.transform.FindChild("btnAceptar").GetComponent<Button>();
        btnAceptarIndicacionBaul = popIndicacionBaul.transform.FindChild("btnAceptar").GetComponent<Button>();
        btnAceptarFinReto = popFinReto.transform.FindChild("btnAceptar").GetComponent<Button>();
        btnBaul1 = seleccionarBaul.transform.FindChild("btnBaul1").GetComponent<Button>();
        btnBaul2 = seleccionarBaul.transform.FindChild("btnBaul2").GetComponent<Button>();
        btnBaul3 = seleccionarBaul.transform.FindChild("btnBaul3").GetComponent<Button>();


        //Funciones boton
        btnAceptarIndicacionReto.onClick.AddListener(delegate {
            cerrarPopUp("AceptarIndicacionReto");
            banderaIndicacion = 1;
            obtenerPlayerPrefs();
        });
        btnAceptarIndicacionBaul.onClick.AddListener(delegate { cerrarPopUp("AceptarIndicacionBaul"); });
        btnAceptarFinReto.onClick.AddListener(delegate
        {
            cerrarPopUp("AceptarFinReto");
            menuScript.abrirReto(8);
        });
        btnBaul1.onClick.AddListener(delegate { abrirPantalla(2); });
        btnBaul2.onClick.AddListener(delegate { abrirPantalla(3); });
        btnBaul3.onClick.AddListener(delegate { abrirPantalla(4); });
    }

    //Funcion botones mostrar pantalla
    public void abrirPantalla(int pantalla)
    {
        desactivarPantallas();

        switch (pantalla)
        {            
            case 2:
                baules[0].SetActive(true);
                mostrarBaul(1);
                break;
            case 3:
                baules[1].SetActive(true);
                mostrarBaul(2);
                break;
            case 4:
                baules[2].SetActive(true);
                mostrarBaul(3);
                break;
        }
    }


    //Desactivar pantallas
    private void desactivarPantallas()
    {
        seleccionarBaul.SetActive(false);
        for (var index = 0; index < baules.Count; index++) baules[index].SetActive(false);
    }

    //Mostrar PopUp de Indicacion
    private void mostrarIndicacion()
    {
        soundManager.playSound("Sound_OpenPopUp_10", 1);
        switch (banderaIndicacion)
        {
            case 1:
                popIndicacionReto.SetActive(true);
                break;
            case 2:
                popIndicacionBaul.SetActive(true);
                break;
        }
    }

    //Cerrar PopUps
    private void cerrarPopUp(string popUp)
    {
        soundManager.playSound("Sound_Cerrar_3", 1);
        switch (popUp)
        {
            case "AceptarIndicacionReto":
                popIndicacionReto.SetActive(false);
                break;
            case "AceptarIndicacionBaul":
                popIndicacionBaul.SetActive(false);
                break;
            case "AceptarFinReto":
                popFinReto.SetActive(false);
                break;
        }
    }


    //Mostrar contenido del baúl
    private void mostrarBaul(int baul)
    {
        var index = 0;

        banderaIndicacion = 2;
        index = baul - 1;

        Invoke("mostrarIndicacion", .5f);

        //Hojas
        hoja1 = baules[index].transform.FindChild("hoja1").gameObject;
        hoja2 = baules[index].transform.FindChild("hoja2").gameObject;

        //Fondo pantalla guardar
        fondoGuardar = baules[index].transform.FindChild("fondoGuardar").gameObject;

        //Puntuación
        puntuacion = baules[index].transform.FindChild("puntuacion").gameObject;
        mascarasText = puntuacion.transform.FindChild("Text").GetComponent<Text>();
        mascarasText.text = mascarasTotal.ToString();

        //Input Texts
        inpTitulo = hoja1.transform.FindChild("titulo").FindChild("InpTitulo").GetComponent<InputField>();
        inpPersonajes = hoja1.transform.FindChild("personajes").FindChild("InpPersonajes").GetComponent<InputField>();
        inpEscenario = hoja1.transform.FindChild("escenario").FindChild("InpEscenario").GetComponent<InputField>();
        inpDialogos = hoja1.transform.FindChild("dialogos").FindChild("InpDialogos").GetComponent<InputField>();
        inpDialogos2 = hoja2.transform.FindChild("dialogos").FindChild("InpDialogos").GetComponent<InputField>();


        //PopUps
        popElementosGuion = baules[index].transform.FindChild("ElementosGuion").gameObject;
        popZonaTitulo = baules[index].transform.FindChild("popZonaTitulo").gameObject;
        popZonaPersonajes = baules[index].transform.FindChild("popZonaPersonajes").gameObject;
        popZonaEscenario = baules[index].transform.FindChild("popZonaEscenario").gameObject;
        popZonaDialogos = baules[index].transform.FindChild("popZonaDialogos").gameObject;
        popGuardar = baules[index].transform.FindChild("popGuardar").gameObject;
        popFelicidades = baules[index].transform.FindChild("popFelicidades").gameObject;
        popCerca = baules[index].transform.FindChild("popCerca").gameObject;
        popError = baules[index].transform.FindChild("popError").gameObject;

        //Botones
        btnGuion = baules[index].transform.FindChild("btnGuion").GetComponent<Button>();
        btnAceptarGuion = popElementosGuion.transform.FindChild("btnAceptar").GetComponent<Button>();
        btnAceptarTitulo = popZonaTitulo.transform.FindChild("btnAceptar").GetComponent<Button>();
        btnAceptarPersonajes = popZonaPersonajes.transform.FindChild("btnAceptar").GetComponent<Button>();
        btnAceptarEscenario = popZonaEscenario.transform.FindChild("btnAceptar").GetComponent<Button>();
        btnAceptarDialogos = popZonaDialogos.transform.FindChild("btnAceptar").GetComponent<Button>();
        btnAceptarGuardar = popGuardar.transform.FindChild("btnAceptar").GetComponent<Button>();
        btnAceptarFelicidades = popFelicidades.transform.FindChild("btnAceptar").GetComponent<Button>();
        btnAceptarCerca = popCerca.transform.FindChild("btnAceptar").GetComponent<Button>();
        btnAceptarError = popError.transform.FindChild("btnAceptar").GetComponent<Button>();
        btnZonaTitulo = hoja1.transform.FindChild("btnZonaTitulo").GetComponent<Button>();
        btnZonaPersonajes = hoja1.transform.FindChild("btnZonaPersonajes").GetComponent<Button>();
        btnZonaEscenario = hoja1.transform.FindChild("btnZonaEscenario").GetComponent<Button>();
        btnZonaDialogos = hoja1.transform.FindChild("btnZonaDialogos").GetComponent<Button>();
        btnFlechaDer = hoja1.transform.FindChild("btnFlechaDer").GetComponent<Button>();
        btnFlechaIzq = hoja2.transform.FindChild("btnFlechaIzq").GetComponent<Button>();
        btnFin = baules[index].transform.FindChild("btnFin").GetComponent<Button>();
        btnGuardar = baules[index].transform.FindChild("btnGuardar").GetComponent<Button>();

        //Funcionalidad de botones

        //Hoja 1
        btnZonaTitulo.onClick.AddListener(delegate
        {
            popZonaTitulo.SetActive(true);
            soundManager.playSound("Sound_OpenPopUp_10", 1);
        });
        btnZonaPersonajes.onClick.AddListener(delegate
        {
            popZonaPersonajes.SetActive(true);
            soundManager.playSound("Sound_OpenPopUp_10", 1);
        });
        btnZonaEscenario.onClick.AddListener(delegate
        {
            popZonaEscenario.SetActive(true);
            soundManager.playSound("Sound_OpenPopUp_10", 1);
        });
        btnZonaDialogos.onClick.AddListener(delegate
        {
            popZonaDialogos.SetActive(true);
            soundManager.playSound("Sound_OpenPopUp_10", 1);
        });
        btnFlechaDer.onClick.AddListener(delegate
        {
            hoja1.SetActive(false);
            hoja2.SetActive(true);
            inpDialogos2.Select();
        });

        //Hoja 2
        btnFlechaIzq.onClick.AddListener(delegate
        {
            hoja2.SetActive(false);
            hoja1.SetActive(true);
        });

        //Baul
        btnGuion.onClick.AddListener(delegate
        {
            popElementosGuion.SetActive(true);
            soundManager.playSound("Sound_OpenPopUp_10", 1);
        });
        btnFin.onClick.AddListener(delegate { evaluarReto(); });
        btnGuardar.onClick.AddListener(delegate
        {
            btnGuardar.gameObject.SetActive(false);
            puntuacion.SetActive(false);
            StartCoroutine(ScreenshotEncode());
        });

        //Pop Ups
        btnAceptarGuion.onClick.AddListener(delegate
        {
            popElementosGuion.SetActive(false);
            soundManager.playSound("Sound_Cerrar_3", 1);
        });
        btnAceptarTitulo.onClick.AddListener(delegate
        {
            popZonaTitulo.SetActive(false);
            soundManager.playSound("Sound_Cerrar_3", 1);
            btnZonaTitulo.gameObject.SetActive(false);
            inpTitulo.Select();
        });
        btnAceptarPersonajes.onClick.AddListener(delegate
        {
            popZonaPersonajes.SetActive(false);
            soundManager.playSound("Sound_Cerrar_3", 1);
            btnZonaPersonajes.gameObject.SetActive(false);
            inpPersonajes.Select();
        });
        btnAceptarEscenario.onClick.AddListener(delegate
        {
            popZonaEscenario.SetActive(false);
            soundManager.playSound("Sound_Cerrar_3", 1);
            btnZonaEscenario.gameObject.SetActive(false);
            inpEscenario.Select();
        });
        btnAceptarDialogos.onClick.AddListener(delegate
        {
            popZonaDialogos.SetActive(false);
            soundManager.playSound("Sound_Cerrar_3", 1);
            btnZonaDialogos.gameObject.SetActive(false);
            inpDialogos.Select();
            btnFlechaDer.gameObject.SetActive(true);
            btnFin.gameObject.SetActive(true);
        });
        btnAceptarGuardar.onClick.AddListener(delegate
        {
            popGuardar.SetActive(false);
            puntuacion.SetActive(true);
            soundManager.playSound("Sound_Cerrar_3", 1);
            mascarasTotal++;
            mascarasText.text = mascarasTotal.ToString();
            PlayerPrefs.SetInt("mascarasODA1_ODA4", mascarasTotal);
            mascaraFeliz.SetActive(true);
            Invoke("ocultarMascara", 1.5f);
            Invoke("finalizarReto", 1.8f);
        });
        btnAceptarFelicidades.onClick.AddListener(delegate
        {
            popFelicidades.SetActive(false);
            soundManager.playSound("Sound_Cerrar_3", 1);
            menuScript.abrirReto(8);
        });
        btnAceptarCerca.onClick.AddListener(delegate
        {
            popCerca.SetActive(false);
            soundManager.playSound("Sound_Cerrar_3", 1);
            menuScript.abrirReto(8);
        });
        btnAceptarError.onClick.AddListener(delegate
        {
            popError.SetActive(false);
            soundManager.playSound("Sound_Cerrar_3", 1);
        });
    }

    //Evaluar reto
    private void evaluarReto()
    {
        if ((inpTitulo.text == "") || (inpPersonajes.text == "") || (inpEscenario.text == "") ||
            (inpDialogos.text == ""))
        {
            soundManager.playSound("Sound_incorrecto_19", 1);
            popError.SetActive(true);
        }
        else
        {
            fondoGuardar.SetActive(true);
            btnFlechaIzq.gameObject.SetActive(false);
            btnFlechaDer.gameObject.SetActive(false);
            hoja1.SetActive(true);
            hoja2.SetActive(true);
            hoja1.transform.localPosition = new Vector3(-260, 25, 0);
            hoja2.transform.localPosition = new Vector3(260, 25, 0);
            btnFin.gameObject.SetActive(false);
            btnGuardar.gameObject.SetActive(true);
        }
    }

    //Guardar captura de pantalla

    private const string MediaStoreImagesMediaClass = "android.provider.MediaStore$Images$Media";

    public static string SaveImageToGallery(Texture2D texture2D, string title, string description)
    {
        using (var mediaClass = new AndroidJavaClass(MediaStoreImagesMediaClass))
        {
            using (var cr = Activity.Call<AndroidJavaObject>("getContentResolver"))
            {
                var image = Texture2DToAndroidBitmap(texture2D);
                var imageUrl = mediaClass.CallStatic<string>("insertImage", cr, image, title, description);
                return imageUrl;
            }
        }
    }

    public static AndroidJavaObject Texture2DToAndroidBitmap(Texture2D texture2D)
    {
        byte[] encoded = texture2D.EncodeToPNG();
        using (var bf = new AndroidJavaClass("android.graphics.BitmapFactory"))
        {
            return bf.CallStatic<AndroidJavaObject>("decodeByteArray", encoded, 0, encoded.Length);
        }
    }

    public static AndroidJavaObject Activity
    {
        get
        {
            if (_activity == null)
            {
                var unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
                _activity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
            }
            return _activity;
        }
    }


    IEnumerator ScreenshotEncode()
    {

        yield return new WaitForEndOfFrame();
        texture1 = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);
        texture1.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
        texture1.Apply();
        SaveImageToGallery(texture1, "CapturaOda4", "");
        soundManager.playSound("Sound_OpenPopUp_10", 1);
        popGuardar.SetActive(true);
    }

    private void obtenerPlayerPrefs()
    {
        mascarasTotal = PlayerPrefs.GetInt("mascarasODA1_ODA4");
    }

    private void ocultarMascara()
    {
        mascaraFeliz.SetActive(false);
    }

    private void finalizarReto()
    {
        if (mascarasTotal >= 5)
        {
            soundManager.playSound("FuegosArtificiales", 1);
            popFelicidades.SetActive(true);
        }
        else
        {
            soundManager.playSound("Sound_OpenPopUp_10", 1);
            popCerca.SetActive(true);
        }
    }

}