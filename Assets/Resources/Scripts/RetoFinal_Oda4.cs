﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RetoFinal_Oda4 : MonoBehaviour
{
    //Colores preguntas
    private readonly Color answerSettedColor = new Color32(14, 209, 242, 255);

    //Colores resultados
    private readonly Color incorrecta = new Color32(255, 51, 51, 255);
    private readonly Color correcta = new Color32(83, 186, 66, 255);

    //popUpsButtons
    private Button btnAtras;
    private Button btnAceptar;
    private Button btnSalir;
    private readonly List<Button> btnRespuestas = new List<Button>();
    private readonly Color defaultAnswerColor = new Color32(9, 74, 139, 255);

    //Scripts ayuda
    private Menu_Oda4 menuScript;

    //popUps
    private readonly List<GameObject> popUps = new List<GameObject>();
    private int preguntaActual;

    //Variables de evaluacion
    private List<Question> preguntas = new List<Question>();
    private List<Answer> respuestas = new List<Answer>();

    //Sonidos
    private SoundManager soundManager;

    private Text txtCalificacion;
    private Text txtRespuesta;
    private Text txtRespuesta2;
    private Text indexp;

    //popUpsText
    private Text txtPregunta;
    private readonly List<Text> txtRespuestas = new List<Text>();

    // ------------------------Genericos Unity----------------------
    private void Start()
    {
        //Scripts de Ayuda
        menuScript = transform.GetComponent<Menu_Oda4>();

        //Sonidos
        soundManager = transform.GetComponent<SoundManager>();

        //popUps
        popUps.Add(menuScript.retos[3].transform.FindChild("popUpPregunta").gameObject);
        popUps.Add(menuScript.retos[3].transform.FindChild("popUpResultados").gameObject);

        //popUps Buttons
        btnAtras = popUps[0].transform.FindChild("btnAnterior").GetComponent<Button>();
        btnAceptar = popUps[0].transform.FindChild("btnAceptar").GetComponent<Button>();

        btnSalir = popUps[1].transform.FindChild("btnSalir").GetComponent<Button>();

        var goRespuestas = popUps[0].transform.FindChild("respuestas").gameObject;
        btnRespuestas.Add(goRespuestas.transform.FindChild("btnRespuesta1").GetComponent<Button>());
        btnRespuestas.Add(goRespuestas.transform.FindChild("btnRespuesta2").GetComponent<Button>());
        btnRespuestas.Add(goRespuestas.transform.FindChild("btnRespuesta3").GetComponent<Button>());

        //popUps Text
        txtPregunta = popUps[0].transform.FindChild("txtPregunta").GetComponent<Text>();

        txtRespuestas.Add(btnRespuestas[0].transform.GetChild(0).GetComponent<Text>());
        txtRespuestas.Add(btnRespuestas[1].transform.GetChild(0).GetComponent<Text>());
        txtRespuestas.Add(btnRespuestas[2].transform.GetChild(0).GetComponent<Text>());

        txtCalificacion = popUps[1].transform.FindChild("txtCalificacion").GetComponent<Text>();
        txtRespuesta = popUps[1].transform.FindChild("resultadoPregunta").GetComponent<Text>();
        txtRespuesta2 = popUps[1].transform.FindChild("valorPregunta").GetComponent<Text>();
        indexp = popUps[1].transform.FindChild("numPregunta").GetComponent<Text>();

        //Funciones Botones
        btnAtras.onClick.AddListener(delegate { regresar(); });
        btnAceptar.onClick.AddListener(delegate { avanzar(); });

        btnSalir.onClick.AddListener(delegate
        {
            limpiarResultados();
            irMenu();
        });

        btnRespuestas[0].onClick.AddListener(delegate { seleccionarRespuesta(0); });
        btnRespuestas[1].onClick.AddListener(delegate { seleccionarRespuesta(1); });
        btnRespuestas[2].onClick.AddListener(delegate { seleccionarRespuesta(2); });
        //mostrarPregunta ();
    }


    // ------------------------Genericos Retos----------------------

    //Funciones secuencia
    public IEnumerator secuenciaEventos(string secuencia, float tiempoEspera)
    {
        yield return new WaitForSeconds(tiempoEspera);
        switch (secuencia)
        {
            case "iniciarEvaluacion":
                soundManager.playSound("Sound_Fondoevaluacion", 0);
                crearCuestionario();
                seleccionarPreguntasRandom(5);
                preguntaActual = 0;
                popUps[0].SetActive(true);
                popUps[1].SetActive(false);

                limpiarResultados();
                mostrarPregunta();
                break;
            case "evaluar":
                mostrarResultados();
                break;
            case "salir":
                limpiarResultados();
                irMenu();
                break;
        }
    }

    // ------------------------Funciones UI------------------------------
    private void mostrarPregunta()
    {
        txtPregunta.text = "" + (preguntaActual + 1) + ". " + respuestas[preguntaActual].GetQuestion().GetQuestionText();
        txtRespuestas[0].text = "a) " + respuestas[preguntaActual].GetQuestion().GetAnserTexts()[0];
        txtRespuestas[1].text = "b) " + respuestas[preguntaActual].GetQuestion().GetAnserTexts()[1];
        txtRespuestas[2].text = "c) " + respuestas[preguntaActual].GetQuestion().GetAnserTexts()[2];

        colorearRespuestaSeleccionada();
        if (preguntaActual <= 0)
        {
            btnAtras.interactable = false;
        }
        else
        {
            btnAtras.interactable = true;
            btnAtras.transform.GetChild(0).GetComponent<Text>().text = "ANTERIOR";

            btnAceptar.transform.localPosition = new Vector3(216, btnAceptar.transform.localPosition.y,
                btnAceptar.transform.localPosition.z);
        }

        if (preguntaActual >= respuestas.Count - 1)
            btnAceptar.transform.GetChild(0).GetComponent<Text>().text = "TERMINAR";
        else btnAceptar.transform.GetChild(0).GetComponent<Text>().text = "SIGUIENTE";
    }

    private void colorearRespuestaSeleccionada()
    {
        for (var index = 0; index < txtRespuestas.Count; index++)
            if (index == respuestas[preguntaActual].GetAnswer()) txtRespuestas[index].color = answerSettedColor;
            else txtRespuestas[index].color = defaultAnswerColor;
    }

    private void mostrarResultados()
    {
        popUps[0].SetActive(false);
        popUps[1].SetActive(true);

        var correctas = 0;
        indexp.gameObject.SetActive(true);
        txtRespuesta.gameObject.SetActive(true);
        txtRespuesta2.gameObject.SetActive(true);
        for (var index = 0; index < respuestas.Count; index++)
        {
            var txtRespuestaClone = Instantiate(txtRespuesta.gameObject);
            txtRespuestaClone.transform.SetParent(txtRespuesta.transform.parent);
            txtRespuestaClone.transform.localScale = new Vector3(1, 1, 1);
            txtRespuestaClone.transform.localPosition = txtRespuesta.transform.localPosition + Vector3.down * index * 50;

            var txtRespuesta2Clone = Instantiate(txtRespuesta2.gameObject);
            txtRespuesta2Clone.transform.SetParent(txtRespuesta2.transform.parent);
            txtRespuesta2Clone.transform.localScale = new Vector3(1, 1, 1);
            txtRespuesta2Clone.transform.localPosition = txtRespuesta2.transform.localPosition + Vector3.down * index * 50;

            var indexpClone = Instantiate(indexp.gameObject);
            indexpClone.transform.SetParent(indexp.transform.parent);
            indexpClone.transform.localScale = new Vector3(1, 1, 1);
            indexpClone.transform.localPosition = indexp.transform.localPosition + Vector3.down * index * 50;
            indexpClone.GetComponent<Text>().text = index + 1 + ".";

            if (respuestas[index].GetQuestion().ReviewQuestion(respuestas[index].GetAnswer()))
            {
                txtRespuestaClone.GetComponent<Text>().text = "Correcto";
                txtRespuestaClone.GetComponent<Text>().color = correcta;
                txtRespuesta2Clone.GetComponent<Text>().text = "2";
                correctas++;
            }
            else
            {
                txtRespuestaClone.GetComponent<Text>().text = "Incorrecto";
                txtRespuestaClone.GetComponent<Text>().color = incorrecta;
                txtRespuesta2Clone.GetComponent<Text>().text = "0";
            }
        }
        txtRespuesta.gameObject.SetActive(false);
        txtCalificacion.text = "Total" + "                   " + correctas*2 + " / 10";
    }

    private void limpiarResultados()
    {
        foreach (Transform child in popUps[1].transform)
            if (child.name == "resultadoPregunta(Clone)") Destroy(child.gameObject);
    }

    // ------------------------Funciones Botones-------------------------

    private void regresar()
    {
        preguntaActual--;
        mostrarPregunta();
    }

    private void avanzar()
    {
        if (preguntaActual >= respuestas.Count - 1)
        {
            StartCoroutine(secuenciaEventos("evaluar", 0.1f));
        }
        else
        {
            preguntaActual++;
            mostrarPregunta();
        }
    }

    private void seleccionarRespuesta(int boton)
    {
        respuestas[preguntaActual].SetAnswer(boton);
        soundManager.playSound("Sound_SeleccionarRespuesta", 1);

        colorearRespuestaSeleccionada();
    }

    // ------------------------Funciones Evaluacion----------------------

    private void seleccionarPreguntasRandom(int numeroPreguntas)
    {
        respuestas = new List<Answer>();
        for (var i = 0; i < numeroPreguntas; i++)
        {
            var randomIndex = Random.Range(0, preguntas.Count);
            respuestas.Add(new Answer(preguntas[randomIndex], -1));
            preguntas.RemoveAt(randomIndex);
        }
    }

    public void irMenu()
    {
        menuScript.abrirMenu();
        soundManager.playSound("Fondo piano", 0);
    }

    private void crearCuestionario()
    {
        preguntas = new List<Question>();
        preguntas.Add(new Question("Las obras de teatro se dividen en:",
            new[] {"Personajes", "Actos", "Escenas"},
            1));
        preguntas.Add(new Question("Los elementos que forman parte de un guión teatral son:",
            new[]
            {
                "Personajes, escenografía y diálogos",
                "Diálogos, locaciones y director",
                "Personajes y costos de producción"
            },
            0));
        preguntas.Add(new Question("Un guión teatral sirve para que los actores:",
            new[]
            {
                "Sepan dónde serán las locaciones",
                "Memoricen los diálogos",
                "Sepan quién interpretará a los personajes"
            },
            1));
        preguntas.Add(new Question("La descripción de los personajes permite a los actores:",
            new[]
            {
                "Memorizar los diálogos",
                "Saber cuántas veces saldrán a escena",
                "Personificar el papel que les tocó"
            },
            2));
        preguntas.Add(new Question("La opción que describe mejor a un personaje de cuento de hadas:",
            new[]
            {
                "Era una princesa de piernas delgadas.",
                "Era una princesa que vivía en la montaña encantada.",
                "Era una hermosa princesa."
            },
            1));
        preguntas.Add(new Question("¿En cuál descripción se utiliza un adjetivo?",
            new[]
            {
                "Los frijoles mágicos se esparcían.",
                "Los frijoles volaron hacia el monte.",
                "Los frijoles estaban regados por el campo."
            },
            0));
        preguntas.Add(new Question("¿En cuál descripción se utiliza un adjetivo?",
            new[]
            {
                "El búho apareció volando por la cueva.",
                "El búho del bosque voló hacia la cueva.",
                "Ahora, el búho de ojos inmensos volaba."
            },
            2));
        preguntas.Add(new Question("¿En cuál descripción se utiliza un adjetivo?",
            new[]
            {
                "La princesa estaba dentro de la torre.",
                "La princesa estaba rodeada de un dragón.",
                "La princesa estaba rodeada de un temible dragón."
            },
            2));
        preguntas.Add(new Question("¿Cuál es el guión de teatro?",
                new[]
                {
                    "Juan - ¡Sal! ¡Hay una cosa muy rara en el cielo!",
                    "– Juan- ¡Sal! ¡Hay una cosa muy rara en el cielo!",
                    "Juan, - dijo Ana- ¡Sal! ¡Hay una cosa muy rara en el cielo!"
                },
                0));
    }
}

public class Question
{
    private readonly string[] _answersTexts;
    private readonly int _correctAnswer;
    private readonly string _questionText;

    /// <summary>
    ///     Question with only one correct answer
    /// </summary>
    /// <param name="questionTest">Here comes the question string</param>
    /// <param name="answerTexts">Here comes all the text of each answer</param>
    /// <param name="correctAnswer">Correct Answer, NOTE it has to be int starting in 0</param>
    public Question(string questionTest, string[] answerTexts, int correctAnswer)
    {
        _questionText = questionTest;
        _answersTexts = answerTexts;
        _correctAnswer = correctAnswer;
    }

    public bool ReviewQuestion(int answer)
    {
        if (answer == _correctAnswer) return true;
        return false;
    }

    public string GetQuestionText()
    {
        return _questionText;
    }

    public string[] GetAnserTexts()
    {
        return _answersTexts;
    }
}

public class Answer
{
    private int _answer;
    private readonly Question _question;

    /// <summary>
    ///     Answer for a single answer question
    /// </summary>
    /// <param name="question">Question object</param>
    /// <param name="answer">Answer to the question object</param>
    public Answer(Question question, int answer)
    {
        _question = question;
        _answer = answer;
    }

    public Question GetQuestion()
    {
        return _question;
    }

    public int GetAnswer()
    {
        return _answer;
    }

    public void SetAnswer(int answer)
    {
        _answer = answer;
    }
}