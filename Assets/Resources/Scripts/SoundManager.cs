﻿using System.Collections;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    //Reproductores de sonido
    [HideInInspector] private AudioSource[] audios;

    private void Start()
    {
        audios = GetComponents<AudioSource>();
        audios[0].volume = 0.5f;
        audios[0].loop = true;
        audios[1].volume = 1.0f;
        audios[2].volume = 1.0f;
        audios[2].loop = true;
    }

    //Manda a llamar el sonido en el audioSource indicado
    public void playSound(string soundName, int audioSource)
    {
        StartCoroutine(pSound(soundName, audioSource));
    }


    public void stopSound(int audioSource)
    {
        audios[audioSource].Stop();
    }

    private IEnumerator pSound(string soundName, int audioSource)
    {
        var pathToDownload = "";
        if (Application.platform == RuntimePlatform.Android) pathToDownload = Application.persistentDataPath;
        else pathToDownload = Application.streamingAssetsPath;


        //string newName = soundName.Replace(",", " ");
        var audio = new WWW("file://" + pathToDownload + "/" + soundName + ".mp3");
        yield return audio;
        try
        {
            audios[audioSource].Stop();
            audios[audioSource].clip = audio.GetAudioClip(true, true, AudioType.MPEG);
            //print (audio.error);
            audios[audioSource].Play();
        }
        catch
        {
            Debug.Log("Fallo al reproducir el sonido");
        }
    }
}